//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of
//	this software and associated documentation files (the "Software"), to deal in
//	the Software without restriction, including without limitation the rights to use,
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//	Software, and to permit persons to whom the Software is furnished to do so,
//	subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;

namespace CodeMules.Promises {
	/// <summary>
	/// A controller so that the owner of an <see cref="IPromise"/> can
	/// deliver it
	/// </summary>
	public interface IPromiseController : IPromise {
		/// <summary>
		/// Get the result of a delivered promise
		/// </summary>
		/// <value>
		/// The result of the promise if the promise is delivered and has a
		/// non-exception result
		/// </value>
		/// <exception name="Exception">The exception from <see cref="Exception"/> when non-null</exception>
		/// <exception name="InvalidOperationException">Thrown when the promise is still pending"</exception>
		/// <exception cref="InvalidOperationException">
		/// Thrown if an attempt is made to deliver the promise after it has already been delivered
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// Thrown if an attempt is made to deliver a value of <c>null</c>. Deliver <see cref="Promise.NullResult"/>
		/// instead
		/// </exception>
		/// <remarks>
		/// The default value for deliver promises is <c>true</c>. 
		/// <para>
		/// Although the value <c>null</c> cannot be delivered directly, it may be indirectly specified by
		/// delivery of <see cref="Promise.NullResult"/>. This allows <c>Result == null</c>
		/// </para>
		/// </remarks>
		object Result { get; }

		/// <summary>
		/// Get the result, as is, never throwing an exception
		/// </summary>
		/// <value>the result value</value>
		/// <remarks>
		/// This value will be <c>null</c> if the promise has not been delivered, and
		/// may also be <c>null</c> when the promise was delivered with <see cref="Promise.NullResult"/>
		/// </remarks>
		object SafeResult { get; }

		/// <summary>
		/// The <see cref="Exception"/> associated with the promise
		/// </summary>
		/// <value>
		/// The exception associated with a failed promise or <c>null</c> if there
		/// is no error
		/// </value>
		/// <exception name="InvalidOperattionException">Thrown when the promise is still pending"</exception>
		Exception Exception { get; }

		/// <summary>
		/// Determine if the promise was delivered successfully
		/// </summary>
		/// <value>The is successful.</value>
		bool IsSuccessful { get; }
		
		/// <summary>
		/// Determine if the promise is canceled
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise is canceled, <c>false</c> otherwise
		/// </value>
		bool IsCanceled { get; }

		/// <summary>
		/// Determine if the promise is delivered with a failure
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise was delivered with an <see cref="System.Exception"/>, 
		/// <c>false</c> if the promise is not delivered, or has a non-exception result
		/// </value>
		bool IsFailed { get; }

		/// <summary>
		/// Determine if the promise has been delivered
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise has been delivered, <c>false</c> if the
		/// result is still pending
		/// </value>
		/// <seealso cref="IsPending"/>
		bool IsDelivered { get; }

		/// <summary>
		/// Determine if the promise is still pending delivery
		/// </summary>
		/// <value>
		/// <c>true</c> if the promise is not delivered, <c>false</c> if the
		/// promise has been delivered
		/// </value>
		/// <seealso cref="IsDelivered"/>
		bool IsPending { get; }

		/// <summary>
		/// Deliver the promise with a result value of <c>true</c>
		/// </summary>
		/// <returns><c>this</c></returns>
		IPromiseController Deliver();

		/// <summary>
		/// Deliver the result of the promise
		/// </summary>
		/// <param name="result">
		/// The result of the promise
		/// </param>
		/// <returns><c>this</c></returns>
		IPromiseController Deliver(object result);

		/// <summary>
		/// Fail the promise
		/// </summary>
		/// <param name="exception">The exception that occurred</param>
		/// <returns><c>this</c></returns>
		IPromiseController Deliver(Exception exception);

		/// <summary>
		/// Deliver the result of this promise as the result of another promise
		/// </summary>
		/// <param name="promise">The promise that has the result being delivered</param>
		/// <remarks>
		/// This is equivalent to:
		/// <c>
		/// Deliver(promise.SafeResult)
		/// </c>
		/// </remarks>
		IPromiseController Deliver(IPromiseController promise);

		/// <summary>
		/// Break the Promise
		/// </summary>
		/// <remarks>
		/// Alias for <see cref="Deliver(System.Exception)"/> where the <c>exception</c> is
		/// always an instance of <see cref="OperationCanceledException"/>
		/// </remarks>
		/// <returns><c>this</c></returns>
		IPromiseController Cancel();

		/// <summary>
		/// Alias for <see cref="Deliver(Exception)"/>
		/// </summary>
		/// <param name="exception">The exception associated with the failure</param>
		/// <returns><c>this</c></returns>
		IPromiseController Fail(Exception exception);
	}

	/// <summary>
	/// A type-centric promise controller so that the owner of an <see cref="IPromise{T}"/> can
	/// deliver it
	/// </summary>
	/// <typeparam name="T">The type of the underlying promise result</typeparam>
	public interface IPromiseController<T> : IPromiseController, IPromise<T> where T : class {
		/// <summary>
		/// Get the result of the promise
		/// </summary>
		/// <value>
		/// The result of the promise if the promise is delivered and has a
		/// non-exception result
		/// </value>
		/// <exception name="Exception">The exception from <see cref="Exception"/> when applicable</exception>
		/// <exception name="InvalidOperationException">Thrown when the promise is still pending"</exception>
		new T Result { get; }

		/// <summary>
		/// Deliver the result of the promise
		/// </summary>
		/// <param name="result">The result of the promise</param>
		/// <returns><c>this</c></returns>
		IPromiseController<T> Deliver(T result) ;

		/// <summary>
		/// Deliver the result of the promise
		/// </summary>
		/// <param name="result">The result of the promise</param>
		/// <returns><c>this</c></returns>
		IPromiseController<T> DeliverSafe(T result);

		/// <summary>
		/// Deliver the result of this promise as the result of another promise
		/// </summary>
		/// <param name="promise">The promise that has the result being delivered</param>
		/// <remarks>
		/// This is equivalent to:
		/// <c>
		/// Deliver(promise.SafeResult)
		/// </c>
		/// </remarks>
		IPromiseController<T> DeliverSafe(IPromiseController<T> promise);

		/// <summary>
		/// Alias for <see cref="Deliver(Exception)"/>
		/// </summary>
		/// <param name="exception">The exception associated with the failure</param>
		/// <returns><c>this</c></returns>
		new IPromiseController<T> Fail(Exception exception);
		
		/// <summary>
		/// Fail the promise
		/// </summary>
		/// <param name="exception">The exception that occurred</param>
		/// <returns><c>this</c></returns>
		new IPromiseController<T> Deliver(Exception exception);
	}
}

