﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of
//	this software and associated documentation files (the "Software"), to deal in
//	the Software without restriction, including without limitation the rights to use,
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
//	Software, and to permit persons to whom the Software is furnished to do so,
//	subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System;

using NUnit.Framework;

using CodeMules.Promises;

namespace CodeMules.Tests.Promises {
	[TestFixture]
	public class TestsOfPromise {
		int whenAlways;
		int whenSucceeded;
		int whenFailed;
		int whenCanceled;

		[SetUp]
		public void Init() {
			whenAlways = whenSucceeded = whenFailed = whenCanceled = 0;
		}

		private void AddListeners(IPromise promise) {
			promise.Always((obj) => whenAlways++).Success((obj) => whenSucceeded++).
			   Catch((ex) => whenFailed++).Canceled(() => whenCanceled++);
		}

		[Test]
		public void TestDelivery() {
			IPromiseController promise = new Promise();

			Assert.IsTrue(promise.IsPending);
			Assert.IsFalse(promise.IsDelivered);
			Assert.IsFalse(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);
			Assert.IsFalse(promise.IsSuccessful);

			promise.Deliver();
			Assert.IsTrue((bool)promise.Result);
			
			Assert.IsTrue(promise.IsDelivered);
			Assert.IsTrue(promise.IsSuccessful);
			Assert.IsFalse(promise.IsPending);
			Assert.IsFalse(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);

			promise = new Promise();

			Assert.IsTrue(promise.IsPending);
			Assert.IsFalse(promise.IsDelivered);
			Assert.IsFalse(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);
			Assert.IsFalse(promise.IsSuccessful);

			promise.Deliver("test");
			Assert.AreEqual("test",promise.Result);
			Assert.IsTrue(promise.IsDelivered);
			Assert.IsTrue(promise.IsSuccessful);
			Assert.IsFalse(promise.IsPending);
			Assert.IsFalse(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);
		}

		[Test]
		public void TestCatchDelivery() {
			IPromiseController promise = new Promise();

			Assert.IsTrue(promise.IsPending);
			Assert.IsFalse(promise.IsDelivered);
			Assert.IsFalse(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);
			Assert.IsFalse(promise.IsSuccessful);

			promise.Deliver(new ArgumentException());
			Assert.IsNotNull(promise.Exception);
			Assert.IsAssignableFrom(typeof(ArgumentException), promise.Exception);
			Assert.IsNotNull(promise.SafeResult);
			Assert.IsAssignableFrom(typeof(ArgumentException), promise.SafeResult);

			Assert.IsTrue(promise.IsDelivered);
			Assert.IsFalse(promise.IsSuccessful);
			Assert.IsFalse(promise.IsPending);
			Assert.IsTrue(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);

			promise = new Promise();

			Assert.IsTrue(promise.IsPending);
			Assert.IsFalse(promise.IsDelivered);
			Assert.IsFalse(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);
			Assert.IsFalse(promise.IsSuccessful);

			promise.Deliver(new OperationCanceledException());
			Assert.IsNotNull(promise.Exception);
			Assert.IsAssignableFrom(typeof(OperationCanceledException), promise.Exception);
			Assert.IsNotNull(promise.SafeResult);
			Assert.IsAssignableFrom(typeof(OperationCanceledException), promise.SafeResult);

			Assert.IsTrue(promise.IsDelivered);
			Assert.IsFalse(promise.IsSuccessful);
			Assert.IsFalse(promise.IsPending);
			Assert.IsTrue(promise.IsFailed);
			Assert.IsTrue(promise.IsCanceled);

			promise = new Promise();
			promise.Cancel();
			Assert.IsNotNull(promise.Exception);
			Assert.IsAssignableFrom(typeof(OperationCanceledException), promise.Exception);
			Assert.IsNotNull(promise.SafeResult);
			Assert.IsAssignableFrom(typeof(OperationCanceledException), promise.SafeResult);

			Assert.IsTrue(promise.IsDelivered);
			Assert.IsFalse(promise.IsSuccessful);
			Assert.IsFalse(promise.IsPending);
			Assert.IsTrue(promise.IsFailed);
			Assert.IsTrue(promise.IsCanceled);

			promise = new Promise();

			promise.Fail(new ArgumentException());
			Assert.IsNotNull(promise.Exception);
			Assert.IsAssignableFrom(typeof(ArgumentException), promise.Exception);
			Assert.IsNotNull(promise.SafeResult);
			Assert.IsAssignableFrom(typeof(ArgumentException), promise.SafeResult);

			Assert.IsTrue(promise.IsDelivered);
			Assert.IsFalse(promise.IsSuccessful);
			Assert.IsFalse(promise.IsPending);
			Assert.IsTrue(promise.IsFailed);
			Assert.IsFalse(promise.IsCanceled);
		}

		[Test]
		public void TestSuccess() {
			IPromiseController promise = new Promise();

			AddListeners(promise);

			promise.Deliver();

			Assert.AreEqual(1, whenAlways);
			Assert.AreEqual(1, whenSucceeded);
			Assert.AreEqual(0, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Always((obj) => whenAlways++);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(1, whenSucceeded);
			Assert.AreEqual(0, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Success((obj) => whenSucceeded++);
			Assert.AreEqual(2, whenSucceeded);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(0, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Catch((ex) => whenFailed++);
			Assert.AreEqual(2, whenSucceeded);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(0, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Canceled(() => whenCanceled++);
			Assert.AreEqual(2, whenSucceeded);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(0, whenFailed);
			Assert.AreEqual(0, whenCanceled);
		}

		[Test]
		public void TestFailed() {
			Promise promise = new Promise();

			AddListeners(promise);

			promise.Deliver(new ArgumentException());

			Assert.AreEqual(1, whenAlways);
			Assert.AreEqual(0, whenSucceeded);
			Assert.AreEqual(1, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Always((obj) => whenAlways++);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(0, whenSucceeded);
			Assert.AreEqual(1, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Success((obj) => whenSucceeded++);
			Assert.AreEqual(0, whenSucceeded);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(1, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Catch((ex) => whenFailed++);
			Assert.AreEqual(0, whenSucceeded);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(2, whenFailed);
			Assert.AreEqual(0, whenCanceled);

			// Test post delivery
			promise.Canceled(() => whenCanceled++);
			Assert.AreEqual(0, whenSucceeded);
			Assert.AreEqual(2, whenAlways);
			Assert.AreEqual(2, whenFailed);
			Assert.AreEqual(0, whenCanceled);
		}

	}
}

